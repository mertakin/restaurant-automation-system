﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmrezervasyon : Form
    {
        public frmrezervasyon()
        {
            InitializeComponent();
        }
        DataTable tablo = new DataTable();
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        OleDbConnection komut = new OleDbConnection();

        private void doldu()//listeye veri çekme ve yenileme
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = baglanti;
            cmd.CommandText = "SELECT * FROM Rezervasyon";
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds, "Rezervasyon");
            dataGridView1.DataSource = ds.Tables["Rezervasyon"];
            baglanti.Close();
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[7].Visible = false;
            dataGridView1.Columns[1].Width = 200;
            dataGridView1.Columns[2].Width = 100;
            dataGridView1.Columns[3].Width = 215;
            dataGridView1.Columns[4].Width = 100;
            dataGridView1.Columns[5].Width = 100;
        }
        string gün = DateTime.Now.ToShortDateString();
        public void bugün()
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM Rezervasyon where TARİH like '%" + gün + "%'", baglanti);
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void frmSifreDegistir_Load(object sender, EventArgs e)
        {
           
            if (baglanti.State == ConnectionState.Closed)
            {
                baglanti.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = baglanti;
                cmd.CommandText = "SELECT * FROM Rezervasyon";
                OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                adpr.Fill(ds, "Rezervasyon");
                dataGridView1.DataSource = ds.Tables["Rezervasyon"];
                baglanti.Close();
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[1].Width = 200;
                dataGridView1.Columns[2].Width = 100;
                dataGridView1.Columns[3].Width = 215;
                dataGridView1.Columns[4].Width = 100;
                dataGridView1.Columns[5].Width = 100;
            }
          
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM Rezervasyon where MasaNo like '%" + textBox1.Text + "%'", baglanti);
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == false && e.KeyChar != (char)08)
            {
                e.Handled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM Rezervasyon where AD_SOYAD like '%" + textBox2.Text + "%'", baglanti);
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM Rezervasyon where SİSTEMTARİHİ like '%" + dateTimePicker1.Text + "%'", baglanti);
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            baglanti.Close();

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seçili Rezervasyon kaydını silmek istediğinize emin misiniz?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DataGridViewRow row = dataGridView1.CurrentRow;

                baglanti.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = baglanti;
                cmd.CommandText = "DELETE FROM Rezervasyon where ID=@numara";
                cmd.Parameters.AddWithValue("@numara", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                baglanti.Close();
                doldu();
                frmAna f2 = (frmAna)Application.OpenForms["frmAna"];
                f2.rezervasyon();
            }
           
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)//tarihe göre rezervasyon arama
        {
            tablo.Clear();
            baglanti.Open();
            OleDbDataAdapter cmd5 = new OleDbDataAdapter("SELECT * FROM Rezervasyon where TARİH between tr1 and tr2", baglanti);
            cmd5.SelectCommand.Parameters.AddWithValue("tr1", dateTimePicker2.Value.ToShortDateString());
            cmd5.SelectCommand.Parameters.AddWithValue("tr2", dateTimePicker3.Value.ToShortDateString());
            cmd5.Fill(tablo);
            dataGridView1.DataSource = tablo;
            baglanti.Close();
        }

        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {
            int sutun = 1;
            int satir = 1;
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            ExcelApp.Workbooks.Add();
            ExcelApp.Visible = true;
            ExcelApp.Worksheets[1].Activate();
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                ExcelApp.Cells[satir, sutun + j].Value = dataGridView1.Columns[j].HeaderText;

            }
            satir++;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    ExcelApp.Cells[satir + i, sutun + j].Value = dataGridView1[j, i].Value;
                }
            }
        }
    }
}
