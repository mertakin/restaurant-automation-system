﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmUrunDuzenle : Form
    {
        public frmUrunDuzenle()
        {
            InitializeComponent();
        }
        #region Degiskenler

        int urunNo = 0;
        object comboValue;

        #endregion
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");

        public int seciliUrunNo;

        int deger = 1;
        void MASA()
        {
            baglanti.Open();
            OleDbCommand komut1 = new OleDbCommand("Select * from Ayar where deger='" + deger + "'", baglanti);
            OleDbDataReader okuyucu = komut1.ExecuteReader();
            while (okuyucu.Read())
            {
                txtmasa.Text = okuyucu["MasaSayisi"].ToString();
            }
            baglanti.Close();
        }
        private void frmUrunDuzenle_Load(object sender, EventArgs e)
        {
            MASA();

            comboDoldur();

         
            DataTable urun = Veritabani.VeriGetir("select * from Urun where SilinmisMi=False");
            DataTable grup = Veritabani.VeriGetir("select * from UrunGrubu");
            for (int groupIndex = 0; groupIndex < grup.Rows.Count; ++groupIndex)
            {
                
                this.listView1.Groups.Add(grup.Rows[groupIndex]["UrunGrupNo"].ToString(), grup.Rows[groupIndex]["Adi"].ToString());

                for (int urunIndex = 0; urunIndex < urun.Rows.Count; ++urunIndex)
                {
                    if (urun.Rows[urunIndex]["UrunGrupNo"].ToString() == grup.Rows[groupIndex]["UrunGrupNo"].ToString())
                    {


                        if (File.Exists(urun.Rows[urunIndex]["ResimYolu"].ToString()))
                        {
                            
                                Image myImage = Image.FromFile(urun.Rows[urunIndex]["ResimYolu"].ToString());
                                imageList1.Images.Add(urun.Rows[urunIndex]["UrunNo"].ToString(), myImage);
                                ListViewItem item = new ListViewItem(urun.Rows[urunIndex]["Adi"].ToString(),
                                    urun.Rows[urunIndex]["UrunNo"].ToString(), this.listView1.Groups[groupIndex]);
                                this.listView1.Items.Insert(0, item);
                               
                            

                        }
                        else 
                        {

                            ListViewItem item = new ListViewItem(urun.Rows[urunIndex]["Adi"].ToString(), urun.Rows[urunIndex]["UrunNo"].ToString(),
                            this.listView1.Groups[groupIndex]);
                            this.listView1.Items.Insert(0, item);                         

                        }

                    }

                }
            }

            if (seciliUrunNo >0)
            {
                listView1.Items[seciliUrunNo - 1].Selected = true;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            if (listView1.SelectedItems.Count > 0)
                urunNo = Convert.ToInt32(listView1.SelectedItems[0].ImageKey);

            DataTable urunDT = Veritabani.VeriGetir("select * from Urun where UrunNo=" + urunNo + "");
            textBox2.Text = urunDT.Rows[0]["Adi"].ToString();
            textBox1.Text = urunDT.Rows[0]["Fiyati"].ToString();
            lblResimYolu.Text = urunDT.Rows[0]["ResimYolu"].ToString();

            Combogrup.SelectedValue = urunDT.Rows[0]["UrunGrupNo"];
            if (File.Exists(lblResimYolu.Text))
            {
                pictureBox1.Image = Image.FromFile(lblResimYolu.Text);
                pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            }
        }
        private void comboDoldur()
        {
            DataTable dt = Veritabani.VeriGetir("select * from UrunGrubu");
            Combogrup.DataSource = dt;
            Combogrup.DisplayMember = "Adi";
            Combogrup.ValueMember = "UrunGrupNo";
            
        }
        private void temizle()
        {
            textBox2.Clear();
            textBox1.Clear();
            pictureBox1.Image = null;
            lblResimYolu.Text = "";
        }
        private void btnDuzenle_Click(object sender, EventArgs e)
        {
            
            if (textBox2.Text == "" || textBox1.Text == "" || lblResimYolu.Text == "" || Combogrup.Text == "")
            {

                if (textBox2.Text == "")
                {
                    MessageBox.Show("Boş Alan Bırakmayınız.");
                }
                if (textBox1.Text == "")
                {
                    MessageBox.Show("Boş Alan Bırakmayınız.");
                }
                if (lblResimYolu.Text == "")
                {
                    MessageBox.Show("Lütfen Bir Resim Yolu Seçiniz.");
                }
                if (Combogrup.Text == "")
                {
                    MessageBox.Show("Lütfen Grup Seçiniz.");
                }
            }
            else
            {
                comboValue = Combogrup.SelectedValue;
                if (Urun.urunDuzenle(urunNo, lblResimYolu.Text, textBox1.Text, comboValue, textBox2.Text))
                {
                    temizle();
                    MessageBox.Show("Ürün başarıyla düzenlendi.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    listView1.Items.Clear();                    
                }
                else
                {

                    temizle();
                    MessageBox.Show("Ürün düzenlenirken hata!!!!.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            listView1.Items.Clear();
            frmUrunDuzenle_Load(null, null);
        }

        private void btnGozat_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Resimi Seçiniz";
            openFileDialog1.Filter = "Tümü|*.png;*.jpg;*ico;*.gif|png|*.png|jpeg|*.jpg|icon|*ico|gif|*.gif";
            openFileDialog1.DefaultExt = "jpg";
            openFileDialog1.RestoreDirectory = false;
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                   
                    pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                    lblResimYolu.Text = openFileDialog1.FileName.ToString();
                    pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                }
                catch 
                {
                    MessageBox.Show("Aynı ürün resmi mevcut");
                }
                
            }
        }

        private void comboUrunGrubu_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboValue = Combogrup.SelectedValue;
        }

        private void txtUrunFiyati_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBox1.Text.IndexOf(',') != -1)
            {
                if (e.KeyChar == (char)44)
                {
                    e.Handled = true;
                }
            }
            kontroller.txtParaKontrol(e);
        }

        private void btnUrunSil_Click(object sender, EventArgs e)
        {

            if (textBox2.Text == "" || textBox1.Text == "" || lblResimYolu.Text == "" || Combogrup.Text == "" || urunNo == 0)
            {
                MessageBox.Show("Lütfen ürün seçiniz!","Uyarı",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                DialogResult sonuc = MessageBox.Show("Ürünü silmek istediğinizden emin misiniz?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (sonuc == DialogResult.Yes)
                {
                    Urun.urunDuzenle(urunNo, true);

                    temizle();
                    listView1.Items.Clear();
                    frmUrunDuzenle_Load(null, null);
                }
            }
                
           
        }
       
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        int ADED;
        private void bunifuThinButton24_Click(object sender, EventArgs e)
        {
            if (txtmasa.Text=="" && txtmasa.Text=="0")
            {
                MessageBox.Show("Lütfen masa adedi belirtin.");
            }
            else
            {
                baglanti.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = baglanti;
                cmd.CommandText = "UPDATE Ayar SET MasaSayisi ='" + txtmasa.Text + "'where ID=@NUMARA";
                cmd.Parameters.AddWithValue("@NUMARA", label1.Text.ToString());
                cmd.ExecuteNonQuery();
                baglanti.Close();
                ADED = Convert.ToInt32(txtmasa.Text);
                MessageBox.Show("Masa adediniz " + ADED + " olarak güncellenmiştir", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void frmUrunDuzenle_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmAna f2 = (frmAna)Application.OpenForms["frmAna"];
            f2.MASADÜZEN();
            this.Hide();
        }
    }
}

