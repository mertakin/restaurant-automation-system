﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;



namespace Restoran_Yönetim_Sistemi
{
    public partial class frmAna : Form
    {
        public frmAna()
        {
            InitializeComponent();
        }
        DataTable tablo = new DataTable();
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        OleDbConnection komut = new OleDbConnection();

        public static int MasaNumarasi;
        public static int masaSayisi;
        public static Boolean ayaraGit;
        int doluMasaSayisi = 0;
        public static frmAna anaFrm;




        public string yetki;
        public void MASADÜZEN() //Veri tabanındaki masa adedine göre yükleme yapılır
        {
            lstMasa.Items.Clear();
            DataTable dt = Veritabani.VeriGetir("select * from Ayar");

            DataTable masa = Veritabani.VeriGetir("select MasaSayisi from Ayar");
            for (int i = 0; i < int.Parse(masa.Rows[0]["MasaSayisi"].ToString()); i++)
            {

                if (kontroller.veriVarmi("select * from Siparis where MasaNo=" + (i + 1) + " AND Hesap=True"))
                {
                    lstMasa.Items.Add((i + 1) + ".Masa");
                    lstMasa.Items[i].ImageKey = "doluMasa.png";
                    doluMasaSayisi++;
                }
                else
                {
                    lstMasa.Items.Add((i + 1) + ".Masa");
                    lstMasa.Items[i].ImageKey = "bosMasa.png";
                }               
            }
            masaSayisi = Convert.ToInt32(masa.Rows[0]["MasaSayisi"].ToString());
        }
        int sayi;
        public void rezervasyon()
        {         
            string tarih = DateTime.Now.ToShortDateString();

            OleDbCommand com = new OleDbCommand("Select COUNT(ID) from Rezervasyon where TARİH='" + tarih +"'", baglanti);
            baglanti.Open();
            sayi = Convert.ToInt32(com.ExecuteScalar());
            button1.Text = "Bu gün " + sayi.ToString() + " adet rezervasyonlu masa vardır.";
            baglanti.Close();
            if (sayi==0)
            {
                button1.Text = "Bu gün tarihli rezervasyonlu masa yoktur.";
            }
            else
            {
                button1.Visible = true;
                
            }
        }
        private void frmAna_Load(object sender, EventArgs e)
        {
            label5.Text = yetki.ToString();
            anaFrm = this;
            MASADÜZEN();
            rezervasyon();
        }



        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                MasaNumarasi = (int.Parse(lstMasa.SelectedIndices[0].ToString()) + 1);
                frmMasa frmMasa = new frmMasa();
                frmMasa.yetkim = label5.Text;
                frmMasa.ShowDialog();
            }
            catch
            {

            }

        }

        private void ürünEkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text=="YÖNETİCİ")
            {
                frmUrunEkle frmUrunEkle = new frmUrunEkle();
                frmUrunEkle.ShowDialog();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }         
        }

        private void ürünDüzenleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text == "YÖNETİCİ")
            {
                frmUrunDuzenle frm = new frmUrunDuzenle();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }           
        }

        private void ürünSilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUrunGoster frmUrunGoster = new frmUrunGoster();
            frmUrunGoster.ShowDialog();
        }




        public void lstMasa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstMasa.SelectedItems.Count > 0)
            {
                int masaNo = Convert.ToInt32(lstMasa.SelectedItems[0].Text.Substring(0, lstMasa.SelectedItems[0].Text.IndexOf('.')));

            }


        }

        private void lstMasa_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lstMasa.FocusedItem.Bounds.Contains(e.Location) == true)
                {
                    contextMenuStrip1.Show(Cursor.Position);
                }
            }
        }

        private void masayiKapatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstMasa.SelectedItems.Count > 0 && lstMasa.SelectedItems[0].ImageKey == "doluMasa.png")
            {
                int masaNo = Convert.ToInt32(lstMasa.SelectedItems[0].Text.Substring(0, lstMasa.SelectedItems[0].Text.IndexOf('.')));
                int siparisNo = Siparis.siparisNoGetir(masaNo);
                if (MessageBox.Show("Eğer masayı kapatırsanız hesap ödenmeden masa içerisindeki ürünler kaldırılacaktır." +
                    "\n(*Raporlarda kapatılan masaların bilgilerini göremezsiniz.)" +
                    "\nDevam etmek istiyor musunuz?", "Uyarı", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (Siparis.siparisUrunTemizle(siparisNo))
                    {

                        if (Siparis.siparisTemizle(siparisNo))
                        {
                            lstMasa.SelectedItems[0].ImageKey = "bosMasa.png";
                            frmAna.anaFrm.lstMasa_SelectedIndexChanged(null, null);
                        }
                    }
                }
            }
        }
        private void satışlarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text=="YÖNETİCİ" || label5.Text == "KASİYER")
            {
                frmSatislar frm = new frmSatislar();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici veya kasiyer yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }       
        
              
        private void yeniKullanıcıOluşturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text == "YÖNETİCİ")
            {
                frmKullaniciEkle frm = new frmKullaniciEkle();
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }      

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void frmAna_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void masaEkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void proğramıYenidenBaşlatToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            Application.Restart();
        }

        private void veriTabanınıYedekleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text == "YÖNETİCİ")
            {
                Veritabani.con.Close();
                baglanti.Close();
                yedekleme frmy = new yedekleme();
                frmy.Show();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void rezervasyonlarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text == "YÖNETİCİ" || label5.Text == "KASİYER")
            {
                frmrezervasyon frm = new frmrezervasyon();
                frm.Show();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici veya kasiyer yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void ürünGruplarıEkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (label5.Text=="YÖNETİCİ")
            {
                frmUrunGrup c = new frmUrunGrup();
                c.Show();
            }
            else
            {
                MessageBox.Show("Bu işlemi sadece yönetici yapabilir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (panel1.Height >= 258)
            {
                timer1.Enabled = false;

            }
            else
            {
                panel1.Height = panel1.Height + 10;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (panel1.Height <= 13)
            {
                timer2.Enabled = false;
                panel1.Visible = false;
            }
            else
            {
                panel1.Height = panel1.Height - 10;

            }

        }

        private void masaAnlamlarıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            if (panel1.Height == 13)
            {
                timer1.Enabled = true;
              
            }
            else
            {
                timer2.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (panel1.Height >= 258)
            {
                timer2.Enabled = true;

            }
            else
            {
                panel1.Height = panel1.Height + 10;
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
          
            if (panel2.Width >= 243)
            {
                timer3.Stop();
                timer3.Enabled = false;
            }
            else
            {
                label3.Visible = true;
                panel2.Width = panel2.Width + 5;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            
            if (panel2.Width <= 47)
            {
                timer4.Stop();
            }
            else
            {

                label3.Visible = false;
                panel2.Width = panel2.Width - 5;
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (panel2.Width==47)
            {
                timer3.Start();
            }
            else
            {
                timer4.Start();
            }
        }

        private void rezervasyonTarihiBugünOlanlarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sayi==0)
            {

            }
            else
            {
                if (MessageBox.Show("Bu gün tarihli rezervasyonlu masaları görmek istermisiniz?","SİSTEM",MessageBoxButtons.YesNo,MessageBoxIcon.Information)==DialogResult.Yes)
                {
                    frmrezervasyon göster = new frmrezervasyon();
                    göster.Show();
                    frmrezervasyon f2 = (frmrezervasyon)Application.OpenForms["frmrezervasyon"];
                    f2.bugün();
                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

}
