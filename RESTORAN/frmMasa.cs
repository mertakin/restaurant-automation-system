﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmMasa : Form
    {
        public frmMasa()
        {
            InitializeComponent();
        }
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        int masaNo;

        private void urunGoster()
        {
            DataTable urun = Veritabani.VeriGetir("select * from Urun where SilinmisMi=False");
            DataTable grup = Veritabani.VeriGetir("select * from UrunGrubu");
            for (int groupIndex = 0; groupIndex < grup.Rows.Count; ++groupIndex)
            {
                
                this.lstUrunler.Groups.Add(grup.Rows[groupIndex]["UrunGrupNo"].ToString(), grup.Rows[groupIndex]["Adi"].ToString());

                for (int urunIndex = 0; urunIndex < urun.Rows.Count; ++urunIndex)
                {
                    if (urun.Rows[urunIndex]["UrunGrupNo"].ToString() == grup.Rows[groupIndex]["UrunGrupNo"].ToString())
                    {


                        if (File.Exists(urun.Rows[urunIndex]["ResimYolu"].ToString()))
                        {
                            Image myImage = Image.FromFile(urun.Rows[urunIndex]["ResimYolu"].ToString());
                            imageList1.Images.Add(urun.Rows[urunIndex]["UrunNo"].ToString(), myImage);
                            ListViewItem item = new ListViewItem(urun.Rows[urunIndex]["Adi"].ToString() + "-" + urun.Rows[urunIndex]["Fiyati"].ToString() + " ₺",
                                urun.Rows[urunIndex]["UrunNo"].ToString(), this.lstUrunler.Groups[groupIndex]);
                            this.lstUrunler.Items.Insert(0, item);
                        }
                        else 
                        {
                            ListViewItem item = new ListViewItem(urun.Rows[urunIndex]["Adi"].ToString() + "-" + urun.Rows[urunIndex]["Fiyati"].ToString() + " ₺", urun.Rows[urunIndex]["UrunNo"].ToString(),
                            this.lstUrunler.Groups[groupIndex]);
                            this.lstUrunler.Items.Insert(0, item);
                            //this.lstUrunler.Groups[groupIndex].Items.Insert(0, item);
                        }

                    }

                }
            }
        }

        private void sepetUrunGoster()
        {
            try
            {
                int siparisNo = Siparis.siparisNoGetir(masaNo);
                if (kontroller.veriVarmi("select * from SiparisUrun where SiparisNo=" + siparisNo + ""))
                {
                    DataTable siparisUrun = Veritabani.VeriGetir("select Urun.UrunNo as UrunNo,SiparisUrun.UrunAdet as UrunAdet,Urun.Adi as Adi,Urun.Fiyati as Fiyati,Urun.ResimYolu as ResimYolu from SiparisUrun inner join Urun on SiparisUrun.UrunNo=Urun.UrunNo where SiparisUrun.SiparisNo=" + siparisNo + "");
                    for (int urunIndex = 0; urunIndex < siparisUrun.Rows.Count; ++urunIndex)
                    {
                        for (int i = 0; i < Convert.ToInt32(siparisUrun.Rows[urunIndex]["UrunAdet"].ToString()); i++)
                        {
                            if (File.Exists(siparisUrun.Rows[urunIndex]["ResimYolu"].ToString()))
                            {
                                Image myImage = Image.FromFile(siparisUrun.Rows[urunIndex]["ResimYolu"].ToString());
                                imageList1.Images.Add(siparisUrun.Rows[urunIndex]["UrunNo"].ToString(), myImage);
                                ListViewItem item = new ListViewItem(siparisUrun.Rows[urunIndex]["Adi"].ToString() + "-" + siparisUrun.Rows[urunIndex]["Fiyati"].ToString() + " ₺",
                                    siparisUrun.Rows[urunIndex]["UrunNo"].ToString(), this.lstUrunler.Groups[0]);
                                this.lstSepet.Items.Insert(0, item);
                                this.lstSepet.Groups[0].Items.Insert(0, item);
                                double fiyati = Convert.ToDouble(item.Text.Substring(item.Text.IndexOf('-') + 1, item.Text.LastIndexOf(' ') - item.Text.LastIndexOf('-')));
                                txtToplam.Text = (Convert.ToDouble(txtToplam.Text) + fiyati).ToString();

                            }
                            else 
                            {
                                ListViewItem item = new ListViewItem(siparisUrun.Rows[urunIndex]["Adi"].ToString() + "-" + siparisUrun.Rows[urunIndex]["Fiyati"].ToString() + " ₺", siparisUrun.Rows[urunIndex]["UrunNo"].ToString(),
                                this.lstSepet.Groups[0]);
                                this.lstSepet.Items.Insert(0, item);
                                this.lstSepet.Groups[0].Items.Insert(0, item);
                                double fiyati = Convert.ToDouble(item.Text.Substring(item.Text.IndexOf('-') + 1, item.Text.LastIndexOf(' ') - item.Text.LastIndexOf('-')));
                                txtToplam.Text = (Convert.ToDouble(txtToplam.Text) + fiyati).ToString();
                            }
                        }


                    }

                }
            }
            catch
            {
                MessageBox.Show("SepetUrunGoster Metodunda hata");
            }


        }


        private void comboDoldur()//comba boxsa verileri ekliyor
        {
            DataTable dt = Veritabani.VeriGetir("select * from OdemeTuru");
            comboOdemeTuru.DataSource = dt;
            comboOdemeTuru.DisplayMember = "Adi";
            comboOdemeTuru.ValueMember = "OdemeTuruNo";
        }
        private void siparisleriKaydet()//masaya ürün ekledikten sonra siparişi kaydediyor
        {
            if (lstSepet.Items.Count > 0)
            {
                if (kontroller.veriVarmi("select * from Siparis where MasaNo=" + masaNo + " AND Hesap=True"))
                {
                    int siparisNo = Siparis.siparisNoGetir(masaNo);
                    if (Siparis.siparisUrunTemizle(siparisNo))
                    {
                        foreach (ListViewItem item in lstSepet.Items)
                        {
                            if (item.Group == lstSepet.Groups[0])
                            {
                                if (!kontroller.veriVarmi("select UrunNo from SiparisUrun where UrunNo=" + item.ImageKey + " AND SiparisNo=" + siparisNo + ""))
                                {

                                    int urunAdeti = 0;
                                    for (int i = 0; i < lstSepet.Groups[0].Items.Count; i++)
                                    {
                                        if (item.ImageKey == lstSepet.Groups[0].Items[i].ImageKey)
                                        {
                                            urunAdeti++;
                                            
                                        }
                                    }
                                    if (Siparis.siparisUrunEkle(siparisNo, item.ImageKey, urunAdeti))
                                    {
                                      
                                    }
                                    else
                                    {
                                        MessageBox.Show("Ürünler eklenirken hata!");
                                    }
                                }

                            }
                            else
                            {

                            }

                        }
                    }
                }
                else
                {
                    if (Siparis.siparisEkle(masaNo))
                    {
                        int siparisNo = Siparis.siparisNoGetir(masaNo);
                        foreach (ListViewItem item in lstSepet.Items)
                        {
                            if (item.Group == lstSepet.Groups[0])
                            {
                                if (!kontroller.veriVarmi("select UrunNo from SiparisUrun where UrunNo=" + item.ImageKey + " AND SiparisNo=" + siparisNo + ""))

                                {

                                    int urunAdeti = 0;
                                    for (int i = 0; i < lstSepet.Groups[0].Items.Count; i++)
                                    {
                                        if (item.ImageKey == lstSepet.Groups[0].Items[i].ImageKey)
                                        {
                                            urunAdeti++;
                                            
                                        }
                                    }
                                    if (Siparis.siparisUrunEkle(siparisNo, item.ImageKey, urunAdeti))
                                    {
                                        
                                    }
                                    else
                                    {
                                        MessageBox.Show("Ürünler eklenirken hata!");
                                    }
                                }
                            }                  
                        }
                    }
                    else
                    {
                        MessageBox.Show("Siparisler eklenirken hata meydana geldi.");
                    }

                }
            }
        }
        public string yetkim;
        private void frmMasa_Load(object sender, EventArgs e)
        {
            label10.Text = yetkim.ToString();
            //dateTimePicker1.Format = DateTimePickerFormat.Custom;
            //dateTimePicker1.CustomFormat = "yyyy/MM/dd HH:mm";

            this.Text = "Masa " + frmAna.MasaNumarasi.ToString();
            masaNo = Convert.ToInt32(this.Text.Substring(Convert.ToInt32(this.Text.IndexOf(' '))));
            grpUrun.Visible = true;
            urunGoster();
            comboDoldur();

            
            this.lstSepet.Groups.Add("0", "Ürünler");
            int siparisNo = Siparis.siparisNoGetir(masaNo);
            if (siparisNo != -1)
            {
                sepetUrunGoster();
            }
            bunifuThinButton23.Visible = false;
            //garsonun hesap kapatma yetkisi sınırlaandırıldı
            if (label10.Text=="GARSON")
            {
                bunifuThinButton21.Visible = false;
                bunifuThinButton22.Visible = false;            
            }
            label10.Visible = false;
        }
        private void lstUrunler_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

        }




        private void lstSepet_DoubleClick(object sender, EventArgs e)
        {
            foreach (ListViewItem seciliItem in lstSepet.SelectedItems)
            {
                double fiyati = Convert.ToDouble(seciliItem.Text.Substring(seciliItem.Text.IndexOf('-') + 1, seciliItem.Text.LastIndexOf(' ') - seciliItem.Text.LastIndexOf('-')));
                txtToplam.Text = (Convert.ToDouble(txtToplam.Text) - fiyati).ToString();
                lstSepet.Items[seciliItem.Index].Remove();
            }

        }

        private void frmMasa_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (lstSepet.Items.Count > 0)
            {
                siparisleriKaydet();
                if (frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey == "bosMasa.png")
                {
                    frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey = "doluMasa.png";
                }
            }
            frmAna.anaFrm.lstMasa_SelectedIndexChanged(null, null);
        }

        int siparisNo;
        double ödeme;
        double kalan;
        double üstü;
        private void btnHesapOde_Click(object sender, EventArgs e)
        {
            if (txtToplam.Text == "0,0")
            {
                MessageBox.Show("Lütfen önce ürün seçip sepete ekleyiniz.", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (txtToplam.Text == "0")
            {
                MessageBox.Show("Lütfen önce ürün seçip sepete ekleyiniz.", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            ödeme = Convert.ToDouble(txtToplam.Text);
            kalan = Convert.ToDouble(txtParaUstu.Text);
            üstü = Convert.ToDouble(textBox2.Text);
            if (lstSepet.Items.Count > 0)
            {
                if (MessageBox.Show("Ödenecek miktar: " + ödeme + " tl dir. Hesabı Ödemek İstiyor Musunuz?", "Hesap Öde", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    MessageBox.Show("Verilen miktar: " + üstü + " tl dir.\n\nPara üstünüz; " + kalan + " tl dir.", "Hesap Özeti", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    siparisleriKaydet();
                    siparisNo = Siparis.siparisNoGetir(masaNo);
                    if (Hesap.hesapOde(siparisNo, comboOdemeTuru.SelectedValue, txtToplam.Text))
                    {
                        if (comboOdemeTuru.Text == "")
                        {
                            comboOdemeTuru.SelectedIndex = 0;
                        }
                        else
                        {
                            comboOdemeTuru.Text.ToString();
                        }
                    }
                    if (frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey == "doluMasa.png")
                    {
                        frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey = "bosMasa.png";                      
                    }
                    lstSepet.Items.Clear();
                    txtToplam.Text = "0,0";
                    textBox2.Text = "0";
                    txtParaUstu.Text = "0";
                }
            }

        }

        private void txtVerilenMiktar_TextChanged(object sender, EventArgs e)//para üstü hesaplanması
        {
            if (textBox2.Text != "")
            {
                txtParaUstu.Text = (Convert.ToDouble(textBox2.Text) - Convert.ToDouble(txtToplam.Text)).ToString();
            }
            else
            {
                txtParaUstu.Text = "0";
            }
        }

        private void txtVerilenMiktar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBox2.Text.IndexOf(',') != -1)
            {
                if (e.KeyChar == (char)44)
                {
                    e.Handled = true;
                }
            }
            kontroller.txtParaKontrol(e);
        }     

        private void txtToplam_TextChanged(object sender, EventArgs e)
        {
            txtVerilenMiktar_TextChanged(null, null);
        }

        private void comboOdemeTuru_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bunifuThinButton22.Visible == true)
            {
                bunifuThinButton24.Visible = true;
                bunifuThinButton23.Visible = true;
                groupBox2.Visible = true;
            }
            else
            {
                bunifuThinButton23.Visible = false;
                bunifuThinButton24.Visible = false;
                groupBox2.Visible = false;
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtToplam.Text == "0,0")
            {

                if (textBox1.Text == "" || maskedTextBox1.Text == "")
                {
                    MessageBox.Show("Rezervasyon bilgilerini boş geçemezsiniz", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    baglanti.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = baglanti;
                    cmd.CommandText = "INSERT INTO Rezervasyon(AD_SOYAD,TELEFON,TARİH,SAAT,MasaNo,SİSTEMTARİHİ)VALUES('" + textBox1.Text + "','" + maskedTextBox1.Text + "','" + dateTimePicker1.Text + "','" + dateTimePicker3.Text + "','" + masaNo + "','" + dateTimePicker2.Text + "')";
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    baglanti.Close();
                    MessageBox.Show("Rezervasyon Kaydınız Yapılmıştır.", "BİLGİLENDİRME", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    if (frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey == "bosMasa.png")
                    {
                        frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey = "Rezervasyon1.png";
                    }
                    frmAna f2 = (frmAna)Application.OpenForms["frmAna"];
                    f2.rezervasyon();
                }
            }
            else
            {
                MessageBox.Show("Rezervasyon kayıt için öncelikle ürün sepete eklemeden yapınız.\nMasa menüsünü kapatıp tekrar masaya tıklayınız.", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }     
        private void button3_Click(object sender, EventArgs e)//rezervasyonun iptal edilmesi
        {
            if (txtToplam.Text == "0,0")
            {
                MessageBox.Show("Rezervasyon kaydınız iptal ediliyor.\nKaydı tamamen silmek için rezervasyon menesüne gidiniz", "Bilgilendirme", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                if (frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey == "Rezervasyon1.png")
                {
                    frmAna.anaFrm.lstMasa.Items[masaNo - 1].ImageKey = "bosMasa.png";

                }
            }
            else
            {
                MessageBox.Show("Rezervasyon kaydını iptal için öncelikle hesabı ödeyiniz.", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }

        private void lstUrunler_MouseClick(object sender, MouseEventArgs e)
        {

        }


        private void button4_Click(object sender, EventArgs e)
        {
            if (txtToplam.Text == "0,0")
            {
                MessageBox.Show("Sepetde kaldırılacak ürün yoktur.", "HATA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
               
                if (lstSepet.Items.Count > 0)
                {
                   
                    lstSepet.Items.Clear();
                    txtToplam.Text = "0,0";
                    txtParaUstu.Text = "0";              
                }
              
            }

        }

        private void lstUrunler_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lstUrunler_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (lstUrunler.SelectedItems.Count > 0)
            {
                foreach (ListViewItem seciliItem in lstUrunler.SelectedItems)
                {
                    double fiyati = Convert.ToDouble(seciliItem.Text.Substring(seciliItem.Text.IndexOf('-') + 1, seciliItem.Text.LastIndexOf(' ') - seciliItem.Text.LastIndexOf('-')));
                    txtToplam.Text = (Convert.ToDouble(txtToplam.Text) + fiyati).ToString();

                    ListViewItem item = new ListViewItem(lstUrunler.Items[seciliItem.Index].Text, lstUrunler.Items[seciliItem.Index].ImageKey, this.lstSepet.Groups[0]);//Ürünü,resmi,grubu bağlıyoruz

                    this.lstSepet.Items.Insert(0, item);//ürün eklenmesi
                                                        

                    lstUrunler.SelectedItems[0].Selected = false;
                }
            }
        }
    }
}

