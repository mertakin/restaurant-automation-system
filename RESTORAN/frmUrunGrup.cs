﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmUrunGrup : Form
    {
        public frmUrunGrup()
        {
            InitializeComponent();
        }
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        DataTable tablo = new DataTable();
        private void btnEkle_Click(object sender, EventArgs e)//grup ekleme
        {

            if (txtAdi.Text == "")
            {
                MessageBox.Show("Ürün Adını Boş Bırakmayınız!");
            }
            else
            {
                if (kontroller.veriVarmi("select Adi from UrunGrubu where Adi='" + txtAdi.Text + "'"))
                {
                    MessageBox.Show("Böyle bir grup vardır.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ComboDoldur();
                }
                else
                {
                    if (Grup.grupEkle(txtAdi.Text))
                    {
                        txtAdi.Clear();
                        MessageBox.Show("Grup oluşturuldu.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ComboDoldur();
                    }
                    else
                    {
                        txtAdi.Clear();
                        MessageBox.Show("Grup oluştururken hata meydana geldi.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

            }


        }
     
        void ComboDoldur()//verileri listeleme
        {
            tablo.Clear();
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = baglanti;
            cmd.CommandText = "SELECT * FROM UrunGrubu";
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds, "UrunGrubu");
            dataGridView1.DataSource = ds.Tables["UrunGrubu"];
            dataGridView1.Columns[0].Width = 100;
            dataGridView1.Columns[1].Width = 322;
            baglanti.Close();
        }


        private void frmUrunGrup_Load(object sender, EventArgs e)
        {
            ComboDoldur();
        }



        private void comboGruplar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        string ADED;
        private void btnDuzenle_Click(object sender, EventArgs e)//grup güncelleme
        {
            if (txtAdi.Text == "")
            {
                if (txtAdi.Text == "")
                {
                    MessageBox.Show("Lütfen Boş Bırakmayınız!");
                }

            }
            else
            {
                if (kontroller.veriVarmi("select Adi from UrunGrubu where Adi='" + txtAdi.Text + "'"))
                {
                    MessageBox.Show("Böyle bir grup vardır.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                   
                    ADED = txtAdi.Text;
                    baglanti.Open();
                    OleDbCommand komut2 = new OleDbCommand("Update UrunGrubu set Adi='" + txtAdi.Text + "' where UrunGrupNo=" + dataGridView1.CurrentRow.Cells[0].Value.ToString() + "", baglanti);
                    komut2.ExecuteNonQuery();
                    baglanti.Close();
                    MessageBox.Show("Yeni Grubun adı: " + ADED + " dir.Grup güncellemesi yapılmıştır.", "ÜRÜN GRUP İSMİ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    ComboDoldur();
                }
            }

        }
  
        private void comboGruplar_Click(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)//griddeki veriyi texte alma
        {
            txtAdi.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
        }
        string tarih;
        private void button1_Click(object sender, EventArgs e)//grubu silme
        {
            if (txtAdi.Text=="")
            {
                MessageBox.Show("Lütfen listeden grup adı seçiniz.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                tarih = txtAdi.Text;
                if (MessageBox.Show("Grubun adı: " + tarih + " kaydını silmek istediğinize emin misiniz?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (baglanti.State == ConnectionState.Closed)
                    {
                        baglanti.Open();
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.Connection = baglanti;
                        cmd.CommandText = "DELETE FROM UrunGrubu where UrunGrupNo=@numara";
                        cmd.Parameters.AddWithValue("@numara", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        baglanti.Close();
                        ComboDoldur();                       
                    }
                }
            }           
        }      
    }
}
