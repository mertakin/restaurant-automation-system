﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{

    
    public partial class frmGiris : Form
    {
        public static bool durum;
        public static string kulAdi;
        public frmGiris()
        {
            InitializeComponent();
        }
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        private void frmGiris_Resize(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void btnGiris_Click(object sender, EventArgs e)
        { 
            if (txtKullaniciAdi.Text=="" || comboBox1.Text=="")
            {
                MessageBox.Show("Alanları Boş Geçmeyiniz!");
            }
            else if (txtSifre.Text == "")
            {
                 MessageBox.Show("Şifreyi Boş Geçmeyiniz!");
            }
            else
            {
                baglanti.Open();
                OleDbCommand komut1 = new OleDbCommand("select * from Kullanıcı where ÜYE='" + txtKullaniciAdi.Text + "'", baglanti);
                OleDbDataReader okuyucu1 = komut1.ExecuteReader();
                if (okuyucu1.Read() == true)
                {                 
                    if (comboBox1.Text == okuyucu1["YETKİSİ"].ToString() && txtKullaniciAdi.Text == okuyucu1["ÜYE"].ToString() && txtSifre.Text == okuyucu1["ŞİFRE"].ToString())
                    {
                        MessageBox.Show("Hoşgeldiniz Sayın " + okuyucu1["ÜYE"].ToString());                     
                        frmAna frm1 = new frmAna();
                        frm1.yetki = comboBox1.Text;                                         
                        frm1.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Lütfen bilgilerinizi doğru bir şekilde giriniz", "Yönetim", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                   MessageBox.Show("Lütfen bilgilerinizi doğru bir şekilde giriniz", "Yönetim", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            baglanti.Close();
        }
    
        private void frmGiris_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!durum)
            {
                Application.Exit(); 
            }
        }

        private void frmGiris_Load(object sender, EventArgs e)
        {        
            panel1.BackColor = Color.FromArgb(100, 0, 0, 0);
        }

        private void btnKapat_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
           

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)//şifremi unuttum
        {
            if (txtKullaniciAdi.Text=="")
            {
                MessageBox.Show("Bilgilerinizi kurtarmamız için kullanıcı adınızı girmeniz gerekmektedir.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                baglanti.Open();
                OleDbCommand DT = new OleDbCommand("Select * FROM Kullanıcı where ÜYE='" + txtKullaniciAdi.Text + "'", baglanti);
                OleDbDataReader okuyucu = DT.ExecuteReader();
                while(okuyucu.Read())
                {
                    string şifresi,YETKİSİ;
                    şifresi = okuyucu["ŞİFRE"].ToString();
                    YETKİSİ = okuyucu["YETKİSİ"].ToString();
                    MessageBox.Show("Yetkiniz : " + YETKİSİ + "\nŞifreniz : " + şifresi + "\nBilgi kurtarma işlemi tamamlandı.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    

                }
                baglanti.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
