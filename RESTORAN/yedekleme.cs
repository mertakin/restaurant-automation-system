﻿using RESTORAN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Restoran_Yönetim_Sistemi
{
    public partial class yedekleme : Form
    {
        public yedekleme()
        {
            InitializeComponent();
        }
        string tarih = DateTime.Now.ToShortDateString();
        string yol;
        public void klasöroluşturma()
        {
            yol = Application.StartupPath + "\\YEDEK\\" + tarih;
            Directory.CreateDirectory(yol);
        }
        string ytarih = DateTime.Now.ToString();
        private void yedekleme_Load(object sender, EventArgs e)
        {
            button10.Text = Settings1.Default.mailtarih.ToString();
            button8.Text = Settings1.Default.mailadres.ToString();
            button7.Text = Settings1.Default.yedeklemetarih.ToString();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                klasöroluşturma();
                File.Copy(Application.StartupPath + "\\Data.mdb", yol + "\\Data.mdb");
                Settings1.Default.yedeklemetarih = ytarih;
                Settings1.Default.Save();
                button7.Text = Settings1.Default.yedeklemetarih.ToString();
                MessageBox.Show("Veritabanınız programın klasör dosyası içerisindeki yedekleme klasöründe bugün tarihli olarak bulunmaktadır","YEDEKLEME BİLGİ",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Bugün veritabanı yedeklemesi yapılmıştır.Veritabanı mevcuttur.");
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            
            string adres = (Application.StartupPath + "\\Data.mdb");
            string mailim, şifre;
            mailim = "yldm.osman.33@gmail.com";
            şifre = "Noreply_33";
            if (bunifuMaterialTextbox1.Text == "")
            {
                MessageBox.Show("Lütfen mail adrsinizi giriniz.");
            }
            else
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(bunifuMaterialTextbox1.Text);

                mail.From = new MailAddress(mailim, şifre);
                mail.Subject = "RESTORAN YAZILIM SİSTEMİ";
                mail.Body = "Rertorant sisteminden yönetici onayı ile veritabanı yedek dosyası sizlere gönderilmiştir.\nEk aşağıda sunulmuştur.";
                mail.Attachments.Add(new Attachment(adres));
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Credentials = new System.Net.NetworkCredential(mailim, şifre);
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Send(mail);
                mail.Dispose();
                Settings1.Default.mailtarih = ytarih;
                Settings1.Default.mailadres = bunifuMaterialTextbox1.Text;
                Settings1.Default.Save();
                button10.Text = Settings1.Default.mailtarih.ToString();
                button8.Text = Settings1.Default.mailadres.ToString();
            }
        }
    }
}
