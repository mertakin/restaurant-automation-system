﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmSatislar : Form
    {
        public frmSatislar()
        {
            InitializeComponent();
        }
        public DataTable dt = new DataTable();
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");
        OleDbCommand komut = new OleDbCommand();

        
        private void frmSatislar_Load(object sender, EventArgs e)
        {
            

            baglanti.Open();
            OleDbCommand komut15 = new OleDbCommand("select Count(*) From  Satislar", baglanti);
            OleDbDataReader oku15 = komut15.ExecuteReader();
            while (oku15.Read())
            {

                btnsatış.Text = oku15[0].ToString();

            }
            baglanti.Close();


            

            DataTable dt= Veritabani.VeriGetir("Select * from Satislar");
            gridSatislar.DataSource = dt;

           
            baglanti.Open();
            OleDbCommand komut7 = new OleDbCommand("Select Sum (Tutar) From Satislar", baglanti);
            OleDbDataReader oku3 = komut7.ExecuteReader();
            while (oku3.Read())
            {
                btnkazanç.Text = oku3[0].ToString() + " ₺";
                
            }
            baglanti.Close();
            
            gridSatislar.Columns[4].Visible = false;
            gridSatislar.Columns[5].Visible =false;
            gridSatislar.Columns[0].Width = 150;
            gridSatislar.Columns[1].Width = 350;
            gridSatislar.Columns[2].Width = 100;
            gridSatislar.Columns[3].Width = 230;


           
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yyyy/MM/dd HH:mm";

            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "yyyy/MM/dd HH:mm";

            dateTimePicker1.Value = DateTime.Now.AddDays(-1);
            dateTimePicker2.Value = DateTime.Now;

            dateTimePicker1_ValueChanged(null, null);
        }


        private void txtMasaNo_TextChanged(object sender, EventArgs e)
        {
            baglanti.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT * FROM Satislar where MasaNumarası like '%" + txtMasaNo.Text + "%'", baglanti);
            OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            adpr.Fill(ds);
            gridSatislar.DataSource = ds.Tables[0];
            baglanti.Close();
          
        }

    

        private void txtMasaNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            kontroller.txtSayiKontrol(e);
        }
        private void gridSatislar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridSatislar.Rows.Count > 0)
            {
                gridSatisDetayUrun.DataSource = Veritabani.VeriGetir("select * from SatisDetayUrun where SiparisNo = " + gridSatislar.SelectedRows[0].Cells[5].Value + "");


                gridSatisDetayUrun.Columns[0].Visible = false;
                gridSatisDetayUrun.Columns[1].Visible = false;
            }
            else
            {
                MessageBox.Show("YANLIŞ");
            }

        }
            

          
        

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
          

            if (txtMasaNo.Text == "")
            {
                DataTable dt = Veritabani.VeriGetir("select * from Satislar where Tarih >= #" + dateTimePicker1.Text + "# and Tarih <= #" + dateTimePicker2.Text + "#  ");
                gridSatislar.DataSource = dt;
                baglanti.Open();
                OleDbCommand komut11 = new OleDbCommand("Select Sum (Tutar) From Satislar where Tarih between tr1 and tr2", baglanti);
                komut11.Parameters.AddWithValue("tr1", dateTimePicker1.Value.ToShortDateString());
                komut11.Parameters.AddWithValue("tr2", dateTimePicker2.Value.ToShortDateString());
                btntarih.Text = komut11.ExecuteScalar() + " ₺";
                baglanti.Close();
            }
            else
            {
                DataTable dt = Veritabani.VeriGetir("select * from Satislar where [MasaNumarası] = " + txtMasaNo.Text + " and Tarih >= #" + dateTimePicker1.Text + "# and Tarih <= #" + dateTimePicker2.Text + "#  ");
                gridSatislar.DataSource = dt;

                baglanti.Open();
                OleDbCommand komut11 = new OleDbCommand("Select Sum (Tutar) From Satislar where Tarih between tr1 and tr2", baglanti);
                komut11.Parameters.AddWithValue("tr1", dateTimePicker1.Value.ToShortDateString());
                komut11.Parameters.AddWithValue("tr2", dateTimePicker2.Value.ToShortDateString());
                btntarih.Text = komut11.ExecuteScalar() + " ₺";
                baglanti.Close();

            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker1_ValueChanged(null, null);
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_TabIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked==true)
            {
                DataTable dt = Veritabani.VeriGetir("Select * from Satislar");
                gridSatislar.DataSource = dt;
            }
            else
            {
                dt.Clear();
            }
                        
        }
    }
}
