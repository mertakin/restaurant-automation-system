﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Restoran_Yönetim_Sistemi
{
    public partial class frmKullaniciEkle : Form
    {
        public frmKullaniciEkle()
        {
            InitializeComponent();
        }
        public OleDbConnection baglanti = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data.mdb");

        void liste()//liste yenileme ve textleri temizleme
        {
            if (baglanti.State == ConnectionState.Closed)//tablo bağlantısı
            {
                comboBox1.Text = "";
                txtKULLANICI.Text = "";
                txtşifre.Text = "";
                txtadsoyad.Text = "";

                baglanti.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = baglanti;
                cmd.CommandText = "SELECT * FROM Kullanıcı";
                OleDbDataAdapter adpr = new OleDbDataAdapter(cmd);
                DataSet ds = new DataSet();
                adpr.Fill(ds, "Kullanıcı");
                dataGridView1.DataSource = ds.Tables["Kullanıcı"];
                baglanti.Close();
            }
        }
        private void btnEkle_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text=="" || txtKULLANICI.Text =="" || txtşifre.Text ==""|| txtadsoyad.Text =="")
            {
                MessageBox.Show("Lütfen boş alanları doldurunuz");
            }
            else
            {
                baglanti.Open();
                OleDbCommand komut1 = new OleDbCommand();
                komut1.Connection = baglanti;
                komut1.CommandText = "INSERT INTO Kullanıcı(YETKİSİ,ÜYE,ŞİFRE,ADSOYAD)VALUES('" + comboBox1.Text + "','" + txtKULLANICI.Text + "','" + txtşifre.Text + "','" + txtadsoyad.Text + "')";
                komut1.ExecuteNonQuery();
                baglanti.Close();
                MessageBox.Show("Kayıt işlemi tamamlandı");
                liste();
            }
            
        }

        private void frmKullaniciEkle_Load(object sender, EventArgs e)
        {
            liste();
        }
 
        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {
            if (txtKULLANICI.Text == "")
            {
                MessageBox.Show("Lütfen listeden bir kayıt seçin.", "SİSTEM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (MessageBox.Show("Kullanıcı silinsinmi?", "SİSTEM", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                {
                    baglanti.Open();
                    OleDbCommand cmd2 = new OleDbCommand();
                    cmd2.Connection = baglanti;
                    cmd2.CommandText = "DELETE FROM Kullanıcı WHERE KullaniciNo=@NUMARA";
                    cmd2.Parameters.AddWithValue("@NUMARA", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    cmd2.ExecuteNonQuery();
                    baglanti.Close();
                    liste();
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            comboBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            txtKULLANICI.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            txtşifre.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            txtadsoyad.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }
    }
}
