# Restaurant Automation System

## Summary

It is an automation application designed to facilitate the work of restaurant managers and employees and to make possible the business follow up effectively.

Since the local market is targeted, the application is Turkish. Upon request, updates can be made for different languages.

## Demonstration

![](demo.gif)



